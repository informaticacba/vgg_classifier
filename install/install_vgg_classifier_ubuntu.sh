#!/bin/bash

# - This script is to be run in a clean Ubuntu Xenial machine, by a sudoer user.
# - Caffe is compiled for CPU use only.

# update repositories
sudo apt-get update

# Caffe  dependencies
sudo apt-get install -y cmake
sudo apt-get install -y pkg-config
sudo apt-get install -y libgoogle-glog-dev
sudo apt-get install -y libhdf5-serial-dev
sudo apt-get install -y liblmdb-dev
sudo apt-get install -y libleveldb-dev
sudo apt-get install -y libprotobuf-dev
sudo apt-get install -y protobuf-compiler
sudo apt-get install -y libopencv-dev
sudo apt-get install -y libatlas-base-dev
sudo apt-get install -y libsnappy-dev
sudo apt-get install -y libgflags-dev
sudo apt-get install -y --no-install-recommends libboost-all-dev

# pip and python dependencies
sudo apt-get install -y python-pip
sudo apt-get install -y python-dev

# download gitlab repo
sudo apt-get install -y wget unzip
cd $HOME
wget https://gitlab.com/vgg/vgg_classifier/-/archive/master/vgg_classifier-master.zip -O /tmp/vgg_classifier.zip
unzip /tmp/vgg_classifier.zip
mv vgg_classifier* vgg_classifier
rm -rf /tmp/vgg_classifier.zip

# setup folders
mkdir $HOME/vgg_classifier/dependencies
mkdir $HOME/vgg_classifier/downloaded

# caffe-backend aditional dependencies
sudo apt-get install -y libzmq-dev
sudo pip install protobuf==3.0.0
# liblinear installed below is also a dependency.
# caffe installed below is also a dependency.
# cpp-netlib installed below is also a dependency.

# cpp-netlib aditional dependencies
sudo apt-get install -y libssl-dev

# download caffe
wget wget https://github.com/BVLC/caffe/archive/1.0.zip
unzip 1.0.zip -d $HOME/vgg_classifier/dependencies/

# download cpp-netlib
wget https://github.com/kencoken/cpp-netlib/archive/0.11-devel.zip
unzip 0.11-devel.zip -d $HOME/vgg_classifier/dependencies/

# download liblinear
wget https://github.com/cjlin1/liblinear/archive/v210.zip
unzip v210.zip -d $HOME/vgg_classifier/dependencies/

# remove zips
rm 0.11-devel.zip 1.0.zip v210.zip

# compile caffe
mv $HOME/vgg_classifier/dependencies/caffe* $HOME/vgg_classifier/dependencies/caffe
cd $HOME/vgg_classifier/dependencies/caffe/
cp Makefile.config.example Makefile.config
sed -i 's/# CPU_ONLY/CPU_ONLY/g' Makefile.config
sed -i 's/\/usr\/include\/python2.7/\/usr\/include\/python2.7 \/usr\/local\/lib\/python2.7\/dist-packages\/numpy\/core\/include/g' Makefile.config
sed -i 's/INCLUDE_DIRS :=/INCLUDE_DIRS := \/usr\/include\/hdf5\/serial\/ /g' Makefile.config
sed -i 's/LIBRARY_DIRS :=/LIBRARY_DIRS := \/usr\/lib\/x86_64-linux-gnu\/hdf5\/serial\/ /g' Makefile.config
sed -i 's/# Configure build/CXXFLAGS += -std=c++11/g' Makefile
make all

# compile cpp-netlib
cd  $HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=/usr/bin/cc -DCMAKE_CXX_COMPILER=/usr/bin/c++ ../
make

# compile liblinear
cd $HOME/vgg_classifier/dependencies/liblinear-210/
make lib
ln -s liblinear.so.3 liblinear.so

# vgg_classifier additional dependencies
sudo apt-get install -y libevent-dev python-tk
sudo pip install gevent==0.13.8 greenlet==0.4.15
sudo pip install pyzmq==17.1.2
sudo pip install numpy==1.11.1
sudo pip install matplotlib==1.5.3

# For the change below, see https://github.com/mistio/mist-ce/issues/434#issuecomment-86484952
sudo sed -i 's|PROTOCOL_SSLv3|PROTOCOL_SSLv23|g' /usr/local/lib/python2.7/dist-packages/gevent/ssl.py

# additional dependencies for vgg_classifier test scripts
#imsearch-tools module, see https://github.com/kencoken/imsearch-tools

# compile and install vgg_classifier
cd $HOME/vgg_classifier
mkdir build
cd build
cmake -DCMAKE_CXX_STANDARD=11 -DCaffe_DIR=$HOME/vgg_classifier/dependencies/caffe/ -DCaffe_INCLUDE_DIR="$HOME/vgg_classifier/dependencies/caffe/include;$HOME/vgg_classifier/dependencies/caffe/build/src" -DLiblinear_DIR=$HOME/vgg_classifier/dependencies/liblinear-210/ -Dcppnetlib_DIR=$HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/build/ ../
make
make install
