vgg_classifier Installers
=========================

These installers are experimental. Please use caution when using them and read this README until the end before attempting the installation.

Ubuntu 16 LTS
-------------

 1. This script is to be run in a clean Ubuntu Xenial machine, by a sudoer user.
 2. Caffe is compiled for CPU use only.
 3. `vgg_classifier` will be installed in the $HOME/vgg_classifier folder.
 4. Should you like to run the tests, you will need the `imsearch-tools` module (https://github.com/kencoken/imsearch-tools).
 5. Remember that before the first use you need to configure `vgg_classifier`. See the `Usage` section of the README in the root folder of this repository.

macOS Sierra v10.13.3
---------------------

**NOTE: This installer no longer works because the dependencies of `vgg_classifier` under Mac now require openssl@1.1. This brakes the compilation of `cppnetlib` (see the list of dependencies at the main README of this repository) and makes the installation very difficult. However, the installer is kept here for reference of advanced users who whish to venture into it.**

 1. This script is VERY EXPERIMENTAL. Please be carefull. Instead of running the full script, you might want to open it in a text editor and run one instruction at a time.
 2. The script assumes Homebrew is available in the system (https://brew.sh/).
 3. Make sure you have enough user priviledges to install software using HomeBrew.
 4. Caffe is compiled for CPU use only.
 5. `vgg_classifier` will be installed in the $HOME/vgg_classifier folder.
 6. Should you like to run the tests, you will need the `imsearch-tools` module (https://github.com/kencoken/imsearch-tools)
 7. Remember that before the first use you need to configure `vgg_classifier`. See the `Usage` section of the README in the root folder of this repository.
